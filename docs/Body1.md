# Body1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the activity. | 
**type** | **str** | Type of activity. For example - Run, Ride etc. | 
**start_date_local** | [**Object**](Object.md) | ISO 8601 formatted date time. | 
**elapsed_time** | **int** | In seconds. | 
**description** | **str** | Description of the activity. | [optional] 
**distance** | **float** | In meters. | [optional] 
**trainer** | **int** | Set to 1 to mark as a trainer activity. | [optional] 
**photo_ids** | [**Object**](Object.md) | List of native photo ids to attach to the activity. | [optional] 
**commute** | **int** | Set to 1 to mark as commute. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

